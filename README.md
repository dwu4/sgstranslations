# SGS Translations #

English translations for SanGuoSha (三国杀)

# Partial List of Dependencies

Should your compilation fail, or your fonts look peculiar, install at least
these fonts and packages before you attempt once more.

    http://mirrors.ctan.org/macros/latex/contrib/nopageno.zip
    http://mirrors.ctan.org/macros/latex/contrib/pbox.zip
    http://mirrors.ctan.org/macros/latex/contrib/easy.zip
    http://mirrors.ctan.org/macros/latex/contrib/wasysym.zip
    http://tug.ctan.org/fonts/wasy2/wasy10.mf
    http://tug.ctan.org/fonts/wasy2/rsym.mf
    http://tug.ctan.org/fonts/wasy2/wasychr.mf
    http://tug.ctan.org/fonts/wasy2/lasychr.mf
    http://tug.ctan.org/fonts/wasy7/wasy7.mf
    http://tug.ctan.org/fonts/wasy2/wasy7.mf
    http://tug.ctan.org/fonts/wasy2/rsym.mf
    http://tug.ctan.org/fonts/wasy2/wasy5.mf
    http://tug.ctan.org/fonts/wasy2/wasy6.mf
    http://tug.ctan.org/fonts/wasy2/wasy7.mf
    http://tug.ctan.org/fonts/wasy2/wasy8.mf
    http://tug.ctan.org/fonts/wasy2/wasy9.mf
    http://tug.ctan.org/fonts/wasy2/wasy10.mf
    http://tug.ctan.org/fonts/wasy2/wasyb10.mf   
    http://tug.ctan.org/fonts/wasy2/wasychr.mf
    http://mirrors.ctan.org/fonts/arev.zip
